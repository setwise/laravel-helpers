<?php

namespace Setwise\Helpers\Tests\Unit\Helpers;

use Setwise\Helpers\Tests\TestCase;

class HelpersTest extends TestCase
{
    public function testHumanize()
    {
        $this->assertEquals('Hello World', humanize('hello_world'));
    }
}
