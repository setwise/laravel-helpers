<?php

namespace Setwise\Helpers\Tests\Unit;

use Setwise\Helpers\Helpers as HelpersClass;
use Setwise\Helpers\HelpersFacade;
use Setwise\Helpers\Tests\TestCase;

class HelpersTest extends TestCase
{
    public function testFacadeCorrectlyApplied()
    {
        $this->assertNotNull(HelpersFacade::getFacadeRoot());
    }

    public function testUserProviderClass()
    {
        $userClass = HelpersClass::UserProviderClass();

        $this->assertEquals(config('auth.providers.users.model'), $userClass);
    }
}
