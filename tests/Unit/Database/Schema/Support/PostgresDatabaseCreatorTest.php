<?php

namespace Tests\Unit\Database\Schema\Support;

use PDO;
use PDOException;
use Setwise\Helpers\Database\Schema\Support\PostgresDatabaseCreator;
use Setwise\Helpers\Tests\TestCase;

class PostgresDatabaseCreatorTest extends TestCase
{
    /** @var \Setwise\Helpers\Database\Schema\Support\PostgresDatabaseCreator */
    private $creator;

    /** @var string */
    private $database = 'doesnt_exist';

    protected function setUp(): void {
        parent::setUp();

        $this->creator = new PostgresDatabaseCreator('127.0.0.1', '5432', 'homestead', 'secret');
    }

    protected function tearDown(): void {
        $this->creator->getConnection()->exec("DROP DATABASE IF EXISTS {$this->database}");

        parent::tearDown();
    }

    public function testGetDriverName() {
        $this->assertEquals('pgsql', $this->creator->getDriverName());
    }

    public function testExists() {
        // This database does not exist yet
        $this->assertFalse($this->creator->exists($this->database));

        // Homestead, by default, creates this one
        $this->assertTrue($this->creator->exists('homestead'));
    }

    public function testCreate() {
        // Try to create a valid database
        $this->assertTrue($this->creator->create($this->database));
        $this->assertTrue($this->creator->exists($this->database));

        // Try to create an invalid database
        $this->assertFalse($this->creator->create("invalid_%#@_database"));
    }

    public function testConfigure() {
        $this->creator->create($this->database);

        // First uninstall the extension
        try {
            $this->creator->getConnection()->exec('DROP EXTENSION Postgis');
        } catch (PDOException $exception) {
            // Do nothing
        }

        // Blow away the connection, replace with one that will throw an error
        $pdoMock = $this->createMock(PDO::class);
        $pdoMock->method('exec')->willThrowException(new PDOException());
        $this->setProtectedProperty($this->creator, 'connection', $pdoMock);
        $this->assertFalse($this->creator->configure($this->database));

        // Replace the connection, and try with success
        $this->creator->connect();
        $this->assertTrue($this->creator->configure($this->database));

        // And try to configure it again, but it will just softly stop as it is already configured
        $this->assertTrue($this->creator->configure($this->database));
    }
}
