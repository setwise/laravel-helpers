<?php

namespace Tests\Unit\Database\Schema\Support;

use PDO;
use Setwise\Helpers\Database\Schema\Support\AbstractDatabaseCreator;
use Setwise\Helpers\Tests\TestCase;

class AbstractDatabaseCreatorTest extends TestCase
{
    /** @var \Setwise\Helpers\Database\Schema\Support\AbstractDatabaseCreator|\PHPUnit\Framework\MockObject\MockObject */
    private $creator;

    protected function setUp(): void {
        parent::setUp();

        $this->creator = $this->getMockForAbstractClass(AbstractDatabaseCreator::class, [], '', $callOriginalConstructor = false);
        $this->creator->method('getDriverName')
            ->willReturn('mysql');
        $this->setProtectedProperty($this->creator, 'host', '127.0.0.1');
        $this->setProtectedProperty($this->creator, 'port', '3306');
        $this->setProtectedProperty($this->creator, 'username', 'homestead');
        $this->setProtectedProperty($this->creator, 'password', 'secret');
    }

    public function testConnectAndGetConnection() {
        $connect = $this->creator->connect('homestead');

        $this->assertNotNull($connect);
        $this->assertInstanceOf(PDO::class, $connect);
        $this->assertEquals($connect, $this->creator->getConnection());
    }

    public function testConfigure() {
        $this->assertEquals(true, $this->creator->configure('na'));
    }
}
