<?php

namespace Tests\Unit\Database\Schema\Support;

use Setwise\Helpers\Database\Schema\Support\MySqlDatabaseCreator;
use Setwise\Helpers\Tests\TestCase;

class MySqlDatabaseCreatorTest extends TestCase
{
    /** @var \Setwise\Helpers\Database\Schema\Support\MySqlDatabaseCreator */
    private $creator;

    /** @var string */
    private $database = 'doesnt_exist';

    protected function setUp(): void {
        parent::setUp();

        $this->creator = new MySqlDatabaseCreator('127.0.0.1', '3306', 'homestead', 'secret');
    }

    protected function tearDown(): void {
        $this->creator->getConnection()->exec("DROP DATABASE IF EXISTS {$this->database}");

        parent::tearDown();
    }

    public function testGetDriverName() {
        $this->assertEquals('mysql', $this->creator->getDriverName());
    }

    public function testExists() {
        // This database does not exist yet
        $this->assertFalse($this->creator->exists($this->database));

        // Homestead, by default, creates this one
        $this->assertTrue($this->creator->exists('homestead'));
    }

    public function testCreate() {
        // Try to create a valid database
        $this->assertTrue($this->creator->create($this->database));
        $this->assertTrue($this->creator->exists($this->database));

        // Try to create an invalid database
        $this->assertFalse($this->creator->create("invalid_%#@_database"));
    }
}
