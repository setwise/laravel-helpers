<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            /*
            |--------------------------------------------------------------------------
            | Table Data
            |--------------------------------------------------------------------------
            |
            | Below is table specific items
            |
            */
            // Index
            $table->id();

            // User authentication material
            $table->string('email');
            $table->string('password');

            /*
            |--------------------------------------------------------------------------
            | Additional Columns and Meta Columns
            |--------------------------------------------------------------------------
            |
            | Below there are additional helpers, relations, indexes, etc.
            | for the migration file
            |
            */
            // Helper fields
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();

            // Relations
            //...

            // Uniques
            $table->unique('email');

            // Indexes
            //...
        });
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }
}
