<?php

namespace Setwise\Helpers\Tests\Fakes\App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{

    use Notifiable;

    /** @var array */
    protected $guarded = [];

    /** @var array */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /** @var array */
    protected $dates = [
        'email_verified_at',
    ];
}
