<?php

namespace Setwise\Helpers\Tests\Feature\Console\Commands;

use Illuminate\Support\Facades\Log;
use Setwise\Helpers\Console\Commands\LogSeed;
use Setwise\Helpers\Tests\TestCase;

class LogSeedTest extends TestCase
{
    public function testCommand()
    {
        $logTypes = [
            'emergency',
            'alert',
            'critical',
            'error',
            'warning',
            'notice',
            'info',
            'debug',
        ];

        foreach ($logTypes as $type) {
            Log::shouldReceive($type)
                ->once()->andReturnSelf();
        }

        $this->artisan(LogSeed::class)
            ->assertExitCode(0)
            ->execute();
    }
}
