<?php

namespace Tests\Feature\Console\Commands;

use Illuminate\Support\Env;
use Illuminate\Support\Facades\Config;
use Setwise\Helpers\Console\Commands\InitializeDb;
use Setwise\Helpers\Database\Schema\Support\MySqlDatabaseCreator;
use Setwise\Helpers\Database\Schema\Support\PostgresDatabaseCreator;
use Setwise\Helpers\Tests\TestCase;

class InitializeDbTest extends TestCase
{
    /** @var string */
    private $database = 'testing_creation';

    protected function tearDown(): void
    {
        // Delete mysql database if it exists
        $connector = new MySqlDatabaseCreator('127.0.0.1', '3306', 'homestead', 'secret');
        $connector->getConnection()->exec("DROP DATABASE IF EXISTS {$this->database}");

        // Delete postgres database if it exists
        $connector = new PostgresDatabaseCreator('127.0.0.1', '5432', 'homestead', 'secret');
        $connector->getConnection()->exec("DROP DATABASE IF EXISTS {$this->database}");

        parent::tearDown();
    }

    public function testCommand()
    {
        Env::getRepository()->set('DB_CONNECTION', 'testing');

        // Should fail on en empty database
        Env::getRepository()->clear('DB_DATABASE');
        $this->artisan(InitializeDb::class)
            ->expectsOutput('Skipping creation of database as DB_DATABASE is empty')
            ->assertExitCode(1)
            ->execute();

        // Should fail, there is no db connection found for the 'testing' connection, which we just removed
        Env::getRepository()->set('DB_DATABASE', $this->database);
        Config::set('database.connections', []);
        $this->artisan(InitializeDb::class)
            ->expectsOutput('Could not find or create creator')
            ->assertExitCode(1)
            ->execute();

        // "Forget" to include the driver within the 'testing' connection
        Config::set('database.connections', [
            'testing' => [
                'driver_missing' => 'invalid',
            ]
        ]);
        $this->artisan(InitializeDb::class)
            ->expectsOutput('Connection driver not set or is empty')
            ->assertExitCode(1)
            ->execute();

        // Restore database connections, try with sqlite... which isn't supported
        Config::set('database.connections', [
            'testing' => [
                'driver' => 'sqlite',
                'database' => ':memory:',
                'prefix' => '',
            ]
        ]);
        $this->artisan(InitializeDb::class)
            ->expectsOutput('Could not find or create creator')
            ->assertExitCode(1)
            ->execute();

        // Change the 'testing' connection to postgres and attempt, but should fail (bad auth)
        Config::set('database.connections', [
            'testing' => [
                'driver' => 'pgsql',
                'host' => '127.0.0.1',
                'port' => '5432',
                'username' => 'invalid',
                'password' => 'invalid'
            ],
        ]);
        $this->artisan(InitializeDb::class)
            ->expectsOutput('Could not find or create creator')
            ->assertExitCode(1)
            ->execute();

        // Change the 'testing' connection to postgres and attempt, fails bad db name
        Env::getRepository()->set('DB_DATABASE', 'invalid_%#@_characters');
        Config::set('database.connections', [
            'testing' => [
                'driver' => 'pgsql',
                'host' => '127.0.0.1',
                'port' => '5432',
                'username' => 'homestead',
                'password' => 'secret'
            ],
        ]);
        $this->artisan(InitializeDb::class)
            ->expectsOutput("An error occurred creating 'invalid_%#@_characters'")
            ->assertExitCode(1)
            ->execute();

        // Change the 'testing' connection to postgres and attempt, succeeds
        Env::getRepository()->set('DB_DATABASE', $this->database);
        Config::set('database.connections', [
            'testing' => [
                'driver' => 'pgsql',
                'host' => '127.0.0.1',
                'port' => '5432',
                'username' => 'homestead',
                'password' => 'secret'
            ],
        ]);
        $this->artisan(InitializeDb::class)
            ->expectsOutput("Successfully created '{$this->database}'")
            ->expectsOutput("Running final configuration on '{$this->database}'")
            ->assertExitCode(0)
            ->execute();

        // Do it again, but the output should say that it is already created
        Env::getRepository()->set('DB_DATABASE', $this->database);
        Config::set('database.connections', [
            'testing' => [
                'driver' => 'pgsql',
                'host' => '127.0.0.1',
                'port' => '5432',
                'username' => 'homestead',
                'password' => 'secret'
            ],
        ]);
        $this->artisan(InitializeDb::class)
            ->expectsOutput("Database '{$this->database}' already exists.")
            ->expectsOutput("Running final configuration on '{$this->database}'")
            ->assertExitCode(0)
            ->execute();

        // Change to using a mysql database, we know error handling works so just try the mysql functionality
        Env::getRepository()->set('DB_DATABASE', $this->database);
        Config::set('database.connections', [
            'testing' => [
                'driver' => 'mysql',
                'host' => '127.0.0.1',
                'port' => '3306',
                'username' => 'homestead',
                'password' => 'secret'
            ],
        ]);
        $this->artisan(InitializeDb::class)
            ->expectsOutput("Successfully created '{$this->database}'")
            ->expectsOutput("Running final configuration on '{$this->database}'")
            ->assertExitCode(0)
            ->execute();
    }
}
