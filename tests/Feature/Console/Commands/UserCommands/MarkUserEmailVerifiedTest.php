<?php

namespace Tests\Feature\Console\Commands\UserCommands;

use Setwise\Helpers\Tests\Fakes\App\Models\User;
use Setwise\Helpers\Console\Commands\UserCommands\MarkUserEmailVerified;
use Setwise\Helpers\Tests\TestCase;

class MarkUserEmailVerifiedTest extends TestCase
{
    public function testCommand()
    {
        $verified = $this->createUser([
            'email_verified_at' => now(),
        ]);
        $notVerified = $this->createUser([
            'email_verified_at' => null,
        ]);

        // Invalid email should fail
        $this->artisan(MarkUserEmailVerified::class, [
            'email' => $this->faker->unique()->email,
        ])->assertExitCode(1)
            ->execute();

        // Valid email, which needs to be verified
        $this->artisan(MarkUserEmailVerified::class, [
            'email' => $notVerified->email,
        ])->expectsOutput("User '{$notVerified->email}' not verified. Verifying...")
            ->assertExitCode(0)
            ->execute();
        $this->assertTrue($notVerified->refresh()->hasVerifiedEmail());

        // Valid email, which is already verified
        $this->artisan(MarkUserEmailVerified::class, [
            'email' => $verified->email,
        ])->expectsOutput("User '{$verified->email}' already verified.")
            ->assertExitCode(0)
            ->execute();
    }
}
