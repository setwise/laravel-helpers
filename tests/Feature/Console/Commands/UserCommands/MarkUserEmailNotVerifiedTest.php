<?php

namespace Tests\Feature\Console\Commands\UserCommands;

use App\Models\User;
use Setwise\Helpers\Console\Commands\UserCommands\MarkUserEmailNotVerified;
use Setwise\Helpers\Tests\TestCase;

class MarkUserEmailNotVerifiedTest extends TestCase
{
    public function testCommand()
    {
        $verified = $this->createUser([
            'email_verified_at' => now(),
        ]);
        $notVerified = $this->createUser([
            'email_verified_at' => null,
        ]);

        // Invalid email should fail
        $this->artisan(MarkUserEmailNotVerified::class, [
            'email' => $this->faker->unique()->email,
        ])->assertExitCode(1)
            ->execute();

        // Valid email, which is already not verified
        $this->artisan(MarkUserEmailNotVerified::class, [
            'email' => $notVerified->email,
        ])->expectsOutput("User '{$notVerified->email}' is already verified.")
            ->assertExitCode(0)
            ->execute();

        // Valid email, which is verified, un-verify it
        $this->artisan(MarkUserEmailNotVerified::class, [
            'email' => $verified->email,
        ])->expectsOutput("User '{$verified->email}' is verified. Removing verification...")
            ->assertExitCode(0)
            ->execute();
        $this->assertFalse($verified->refresh()->hasVerifiedEmail());
    }
}
