<?php

namespace Tests\Feature\Console\Commands\UserCommands;

use Setwise\Helpers\Tests\Fakes\App\Models\User;
use Setwise\Helpers\Console\Commands\UserCommands\MarkAllUserEmailsVerified;
use Setwise\Helpers\Tests\TestCase;

class MarkAllUserEmailsVerifiedTest extends TestCase
{
    public function testCommand()
    {
        // No users in database
        $this->artisan(MarkAllUserEmailsVerified::class)
            ->expectsOutput("All emails are verified.")
            ->assertExitCode(0)
            ->execute();

        // Mark all users as verified
        $users = $this->createUser([
            'email_verified_at' => null,
        ], 5);
        $this->artisan(MarkAllUserEmailsVerified::class)
            ->expectsConfirmation(
                "Are you sure you want to mark {$users->count()} remaining unverified emails verified?",
                'yes'
            )
            ->assertExitCode(0)
            ->execute();
        $users->each(function (User $user) {
            $this->assertTrue($user->refresh()->hasVerifiedEmail());
        });
    }
}
