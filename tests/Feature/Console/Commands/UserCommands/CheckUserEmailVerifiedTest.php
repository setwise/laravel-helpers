<?php

namespace Tests\Feature\Console\Commands\UserCommands;

use Setwise\Helpers\Tests\Fakes\App\Models\User;
use Setwise\Helpers\Console\Commands\UserCommands\CheckUserEmailVerified;
use Setwise\Helpers\Tests\TestCase;

class CheckUserEmailVerifiedTest extends TestCase
{
    public function testCommand()
    {
        $verified = $this->createUser([
            'email_verified_at' => now(),
        ]);
        $notVerified = $this->createUser([
            'email_verified_at' => null,
        ]);

        // Invalid email should fail
        $this->artisan(CheckUserEmailVerified::class, [
            'email' => $this->faker->unique()->email,
        ])->assertExitCode(1)
            ->execute();

        // Valid email, verified
        $this->artisan(CheckUserEmailVerified::class, [
            'email' => $verified->email,
        ])->expectsOutput("Email '{$verified->email}' has been verified.")
            ->assertExitCode(0)
            ->execute();

        // Valid email, not verified
        $this->artisan(CheckUserEmailVerified::class, [
            'email' => $notVerified->email,
        ])->expectsOutput("Email '{$notVerified->email}' has not been verified")
            ->assertExitCode(0)
            ->execute();
    }
}
