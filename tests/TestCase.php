<?php

namespace Setwise\Helpers\Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Orchestra\Testbench\TestCase as TestBench;
use Setwise\Helpers\HelpersFacade;
use Setwise\Helpers\HelpersServiceProvider;
use Setwise\Helpers\Tests\Fakes\App\Models\User as FakeUser;
use Setwise\Helpers\Tests\Traits\Factories;
use Setwise\Helpers\Tests\Traits\ReflectionTrait;

abstract class TestCase extends TestBench
{
    use RefreshDatabase;
    use WithFaker;

    use Factories, ReflectionTrait;

    protected function setUp(): void
    {
        parent::setUp();

        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
    }

    protected function getPackageProviders($app)
    {
        return [HelpersServiceProvider::class];
    }

    protected function resolveApplicationCore($app)
    {
        parent::resolveApplicationCore($app);

        $app->detectEnvironment(function () {
            return 'testing';
        });
    }

    /**
     * @param \Illuminate\Foundation\Application $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $config = $app->get('config');

        $config->set('auth.providers.users.model', FakeUser::class);
        $config->set('logging.default', 'errorlog');
        $config->set('database.default', 'testbench');
        $config->set('database.connections.testbench', [
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
        ]);
    }
}
