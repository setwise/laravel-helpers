<?php

namespace Setwise\Helpers\Tests\Traits;

use Hash;
use Illuminate\Database\Eloquent\Collection;
use Setwise\Helpers\Tests\Fakes\App\Models\User;

trait Factories
{
    /**
     * @param array $config
     * @param int $count
     * @return \Setwise\Helpers\Tests\Fakes\App\Models\User|\Illuminate\Database\Eloquent\Collection
     */
    protected function createUser($config = [], $count = 1)
    {
        $collection = new Collection([]);

        for ($index = 1; $index <= $count; $index++) {
            $collection->push(
                User::create(array_merge([
                    'email' => $this->faker->unique()->email,
                    'password' => Hash::make($this->faker->password),
                    'email_verified_at' => $this->faker->boolean ? now() : null,
                ], $config))
            );
        }

        return $count > 1 ? $collection : $collection->first();
    }
}
