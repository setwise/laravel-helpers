<?php

namespace Setwise\Helpers\Tests\Traits;

use ReflectionClass;
use ReflectionException;

trait ReflectionTrait
{
    /**
     * Sets a private property on a given object via reflection
     *
     * @param $object - instance in which private value is being modified
     * @param $property - property on instance being modified
     * @param $value - new value of the property being modified
     * @return void
     */
    protected function setPrivateProperty($object, $property, $value)
    {
        try {
            $reflection = new ReflectionClass($object);
            $reflectionProperty = $reflection->getProperty($property);
            $reflectionProperty->setAccessible(true);
            $reflectionProperty->setValue($object, $value);
        } catch (ReflectionException $exception) {
            return;
        }
    }

    /**
     * Sets a protected property on a given object via reflection
     *
     * @param $object - instance in which protected value is being modified
     * @param $property - property on instance being modified
     * @param $value - new value of the property being modified
     * @return void
     */
    protected function setProtectedProperty($object, $property, $value)
    {
        try {
            $reflection = new ReflectionClass($object);
            $reflectionProperty = $reflection->getProperty($property);
            $reflectionProperty->setAccessible(true);
            $reflectionProperty->setValue($object, $value);
        } catch (ReflectionException $exception) {
            return;
        }
    }

    /**
     * Call protected/private method of a class
     *
     * @param object $object Instantiated object that we will run method on
     * @param string $methodName Method name to call
     * @param array $parameters Array of parameters to pass into method.
     * @return mixed
     */
    protected function invokeMethod($object, $methodName, array $parameters = [])
    {
        try {
            $reflection = new ReflectionClass(get_class($object));
            $method = $reflection->getMethod($methodName);
            $method->setAccessible(true);

            return $method->invokeArgs($object, $parameters);
        } catch (ReflectionException $e) {
            return null;
        }
    }

    /**
     * Gets a private property on a given object via reflection
     *
     * @param $object - instance in which private value is being modified
     * @param $property - property on instance being modified
     * @return mixed|void
     */
    protected function getPrivateProperty($object, $property)
    {
        try {
            $reflection = new ReflectionClass($object);
            $reflectionProperty = $reflection->getProperty($property);
            $reflectionProperty->setAccessible(true);

            return $reflectionProperty->getValue($object);
        } catch (ReflectionException $exception) {
            return;
        }
    }

    /**
     * Gets a protected property on a given object via reflection
     *
     * @param $object - instance in which protected value is being modified
     * @param $property - property on instance being modified*
     * @return mixed|void
     */
    protected function getProtectedProperty($object, $property)
    {
        try {
            $reflection = new ReflectionClass($object);
            $reflectionProperty = $reflection->getProperty($property);
            $reflectionProperty->setAccessible(true);

            return $reflectionProperty->getValue($object);
        } catch (ReflectionException $exception) {
            return;
        }
    }
}
