# Changelog

All notable changes to `Helpers` will be documented in this file.

## Development
- Added in SoftDeletesRouteBinding
- Added in helpers.php w/ `humanize`
- Ported over console commands 
- Ported over ControllerHasUploads
