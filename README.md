# Setwise Helpers  
  
Setwise's generic package containing helpful Laravel tools

### Installation  
  
Install via the command  
```bash  
composer require setwise/laravel-helpers  
```  
  
### Configuration  
  
To publish this package's config file use the command   
```bash  
php artisan vendor:publish --tag=setwise-helpers-config
```  

### Usage

#### Commands
* `php artisan email:check-verified [email] [email2] [email...]`
Use this command to check if given email(s) are verified

* `php artisan email:verify-all`
Use this command to verify all emails in the database

* `php artisan email:unverify [email] [email2] [email...]`
Use this command to unverify given email(s)

* `php artisan email:verify [email] [email2] [email...]`
Use this command to verify given email(s)

* `php artisan db:initialize`
This command will initialize your active database connection with all the proper extensions and requirements for Setwise

* `php artisan setwise:logseed`
Use this command to insert testing log entries

#### Controller Traits

##### ControllerHasUploads
Desc: This trait adds two methods to any controller action, `uploadFile()` and `deleteFile()` to allow for easy file uploads and storage  
Configuration: In your controller's `__construct()` function, define these parameters  
```php
// Prefix folder to prepend to your uploaded file
protected $uploadBucket = '';

// Class the file is being uploaded for, typically a model
protected $uploadClass = '';

// Authorize string resulting from a $this->can($uploadClass, $uploadAuthorize) to authorize the upload
protected $uploadAuthorize = '';

// Validation rules applied ot the upload
protected $uploadRules = 'required|file|image|max:2048';
```
And add these two routes to `web.php`
```php
Route::post('<URL>', '<Controller>@uploadFile')->name('<Name>');
Route::post('<URL>', '<Controller>@deleteFile')->name('<Name>');          
```

#### Database Macros

##### Macro 1
##### Macro 2
##### Macro 3
##### Macro 4
##### Macro 5

#### Eloquent Traits

##### SoftDeletesRouteBinding
Desc: Soft deleted entries cannot be recovered when passed in the url for restoration by default, 
so this trait allows for the usage of route parameters even on restore routes with deleted models.  
**For this method to work, you MUST have `restore` within your route url**

#### Helpers

* `humanize()`
Humanize takes in an underscore cased string and coverts it to a human readable sentence

### Testing  
  
``` bash  
composer test  
```  
  
### Changelog  
  
Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.  
  
## Credits  

- [Gabe Huinker](https://bitbucket.com/setwise)
- [Nicholas Dykhuizen](https://bitbucket.com/setwise)