<?php

namespace Setwise\Helpers\Console\Commands\UserCommands;

use App\Models\User;
use Illuminate\Console\Command;
use Setwise\Helpers\Helpers;

class MarkAllUserEmailsVerified extends Command
{
    /** @var string */
    protected $signature = 'email:verify-all';

    /** @var string */
    protected $description = 'Mark all users as verified (with confirmation).';

    /**
     * Execute the console command
     *
     * @return mixed
     */
    public function handle()
    {
        $response = 0;
        $class = Helpers::UserProviderClass();
        $users = $class::whereNull('email_verified_at')->get();

        if ($users->count() == 0) {
            $this->line("All emails are verified.");

            return 0;
        }

        $question = "Are you sure you want to mark {$users->count()} remaining unverified emails verified?";
        if ($this->confirm($question)) {
            foreach ($users as $user) {
                $response |= $this->call(MarkUserEmailVerified::class, [
                    'email' => [$user->email]
                ]);
            }
        }

        return $response;
    }
}
