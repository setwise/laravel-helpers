<?php

namespace Setwise\Helpers\Console\Commands\UserCommands;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Setwise\Helpers\Helpers;

class CheckUserEmailVerified extends Command
{
    /** @var string */
    protected $signature = 'email:check-verified
                            {email* : The email addresses to check}';

    /** @var string */
    protected $description = 'Checks if the given user(s) are verified';

    /**
     * Execute the console command
     *
     * @return int
     */
    public function handle()
    {
        $response = 0;

        foreach (Arr::wrap($this->argument("email")) as $email) {
            $response |= $this->checkEmailVerification($email);
        }

        return $response;
    }

    /**
     * @param string $email
     * @return int
     */
    private function checkEmailVerification(string $email)
    {
        $class = Helpers::UserProviderClass();
        $user = $class::where("email", "=", $email)->first();

        if (is_null($user)) {
            $this->line("Email '{$email}' not found.");

            return 1;
        }

        $user->hasVerifiedEmail() ?
            $this->info("Email '{$email}' has been verified.") :
            $this->warn("Email '{$email}' has not been verified");

        return 0;
    }
}
