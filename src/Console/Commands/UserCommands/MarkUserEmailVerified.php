<?php

namespace Setwise\Helpers\Console\Commands\UserCommands;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Setwise\Helpers\Helpers;

class MarkUserEmailVerified extends Command
{
    /** @var string */
    protected $signature = 'email:verify
                            {email* : List of email addresses to mark verified.}';

    /** @var string */
    protected $description = 'Mark the given user as verified';

    /**
     * Execute the console command
     *
     * @return mixed
     */
    public function handle()
    {
        $response = 0;

        foreach (Arr::wrap($this->argument("email")) as $email) {
            $response |= $this->verifyUser($email);
        }

        return $response;
    }

    /**
     * @param string $email
     * @return int
     */
    private function verifyUser(string $email)
    {
        $class = Helpers::UserProviderClass();
        $user = $class::where("email", "=", $email)->first();

        if (is_null($user)) {
            $this->line("Email '{$email}' not found.");

            return 1;
        }

        if ($user->hasVerifiedEmail()) {
            $this->line("User '{$email}' already verified.");

            return 0;
        } else {
            $this->line("User '{$email}' not verified. Verifying...");

            return !$user->markEmailAsVerified();
        }
    }
}
