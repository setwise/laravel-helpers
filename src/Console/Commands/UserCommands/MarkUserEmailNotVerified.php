<?php

namespace Setwise\Helpers\Console\Commands\UserCommands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Setwise\Helpers\Helpers;

class MarkUserEmailNotVerified extends Command
{
    /** @var string */
    protected $signature = 'email:unverify
                            {email* : The email addresses to mark not verified.}';

    /** @var string */
    protected $description = 'Mark the given user as not verified';

    /**
     * Execute the console command
     *
     * @return mixed
     */
    public function handle()
    {
        $response = 0;

        foreach (Arr::wrap($this->argument("email")) as $email) {
            $response |= $this->markNotVerified($email);
        }

        return $response;
    }

    /**
     * @param string $email
     * @return int
     */
    private function markNotVerified(string $email)
    {
        $class = Helpers::UserProviderClass();
        $user = $class::where("email", "=", $email)->first();

        if (is_null($user)) {
            $this->line("Email '{$email}' not found.");

            return 1;
        }

        $user->hasVerifiedEmail() ?
            $this->line("User '{$email}' is verified. Removing verification...") :
            $this->line("User '{$email}' is already verified.");

        return !$user->update([
            'email_verified_at' => null,
        ]);
    }
}
