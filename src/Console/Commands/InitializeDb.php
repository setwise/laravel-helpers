<?php

namespace Setwise\Helpers\Console\Commands;

use Setwise\Helpers\Database\Schema\Support\MySqlDatabaseCreator;
use Setwise\Helpers\Database\Schema\Support\PostgresDatabaseCreator;
use Exception;
use Illuminate\Console\Command;

/**
 * Helper artisan console command to create the application database if it doesn't exist
 */
class InitializeDb extends Command
{
    /** @var string */
    protected $signature = 'db:initialize';

    /** @var string */
    protected $description = 'Create the application database if not already created.';

    /**
     * Execute the console command
     *
     * @return int
     */
    public function handle()
    {
        $database = env('DB_DATABASE', null);
        if (!$database) {
            $this->error('Skipping creation of database as DB_DATABASE is empty');

            return 1;
        }

        $creator = $this->getDatabaseCreator();
        if (is_null($creator)) {
            $this->error('Could not find or create creator');

            return 1;
        }

        $this->info("Using {$creator->getDriverName()}");
        if ($creator->exists($database)) {
            $this->info("Database '{$database}' already exists.");
        } else {
            $this->info("Creating '{$database}'...");

            if ($creator->create($database)) {
                $this->info("Successfully created '{$database}'");
            } else {
                $this->warn("An error occurred creating '{$database}'");

                return 1;
            }
        }

        $this->info("Running final configuration on '{$database}'");
        $creator->configure($database);

        return 0;
    }

    /**
     * @return \Setwise\Helpers\Database\Schema\Support\DatabaseCreatorContract
     */
    public function getDatabaseCreator()
    {
        $connectionName = env('DB_CONNECTION', 'null');
        $connection = config("database.connections.{$connectionName}");

        if (is_null($connection)) {
            $this->error(
                "Config 'database.connections.{$connectionName}' not found for given DB_CONNECTION {$connectionName}"
            );

            return null;
        }

        if (!isset($connection['driver']) || !$connection['driver']) {
            $this->error('Connection driver not set or is empty');

            return null;
        }

        try {
            switch ($connection['driver']) {
                case 'pgsql':
                    return new PostgresDatabaseCreator(
                        $connection['host'],
                        $connection['port'],
                        $connection['username'],
                        $connection['password']
                    );
                case 'mysql':
                    return new MySqlDatabaseCreator(
                        $connection['host'],
                        $connection['port'],
                        $connection['username'],
                        $connection['password']
                    );
                default:
                    $this->error("No DatabaseCreator exists for driver type: {$connection['driver']}.");

                    return null;
            }
        } catch (Exception $exception) {
            return null;
        }
    }
}
