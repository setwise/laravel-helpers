<?php

namespace Setwise\Helpers\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class LogSeed extends Command
{
    /** @var string */
    protected $signature = 'setwise:logseed';

    /** @var string */
    protected $description = 'Add one of each log level into the log';

    /**
     * @return int
     */
    public function handle()
    {
        $message = 'Sample Log Message';

        Log::emergency($message);
        Log::alert($message);
        Log::critical($message);
        Log::error($message);
        Log::warning($message);
        Log::notice($message);
        Log::info($message);
        Log::debug($message);

        return 0;
    }
}
