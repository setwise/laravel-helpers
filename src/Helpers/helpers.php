<?php

if (!function_exists('humanize')) {
    /**
     * Convert underscore case labels to human-readable labels
     *
     * @param string $input
     * @return string
     */
    function humanize(string $input)
    {
        return ucwords(str_replace('_', ' ', $input));
    }
}
