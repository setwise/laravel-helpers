<?php

namespace Setwise\Helpers\Database\Schema\Support;

use PDO;

abstract class AbstractDatabaseCreator implements DatabaseCreatorContract
{
    /** @var string */
    protected $host;

    /** @var string */
    protected $port;

    /** @var string */
    protected $username;

    /** @var string */
    protected $password;

    /** @var PDO */
    protected $connection;

    /**
     * @param string $host
     * @param string $port
     * @param string $username
     * @param string $password
     */
    public function __construct(string $host, string $port, string $username, string $password) {
        $this->host = $host;
        $this->port = $port;
        $this->username = $username;
        $this->password = $password;
        $this->connect('');
    }

    /**
     * Return a connection to the database service
     *
     * @return \PDO
     */
    public function connect($database) {
        $this->connection = new PDO(
            sprintf("%s:host=%s;port=%d;", $this->getDriverName(), $this->host, $this->port), $this->username, $this->password, [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            ]
        );

        return $this->connection;
    }

    /**
     * Return the connection pdo object
     *
     * @return \PDO
     */
    public function getConnection() {
        return $this->connection;
    }

    /**
     * Returns the name of the class's driver
     *
     * @return string
     */
    abstract public function getDriverName();

    /**
     * Returns true if the database exists, false otherwise.
     *
     * @param string $database
     * @return boolean
     */
    abstract public function exists(string $database);

    /**
     * Create a given database name
     *
     * @param string $database
     * @return boolean
     */
    abstract public function create(string $database);

    /**
     * Do any final configuration on a given database
     *
     * @param string $database
     * @return boolean
     */
    public function configure(string $database) {
        return true;
    }
}
