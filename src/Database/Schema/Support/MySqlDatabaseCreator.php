<?php

namespace Setwise\Helpers\Database\Schema\Support;

use PDOException;

class MySqlDatabaseCreator extends AbstractDatabaseCreator
{
    /**
     * Returns the name of the class's driver
     *
     * @return string
     */
    public function getDriverName() {
        return 'mysql';
    }

    /**
     * Returns true if the database exists, false otherwise.
     *
     * @param string $database
     * @return boolean
     */
    public function exists(string $database) {
        $statement = $this->connection->prepare('SHOW DATABASES LIKE ?');
        $statement->execute([$database]);

        return count($statement->fetchAll()) > 0;
    }

    /**
     * Create a given database name
     *
     * @param string $database
     * @return boolean
     */
    public function create($database) {
        try {
            $statement = $this->connection->prepare("CREATE DATABASE IF NOT EXISTS {$database}");

            return boolval($statement->execute());
        } catch (PDOException $exception) {
            return false;
        }
    }
}
