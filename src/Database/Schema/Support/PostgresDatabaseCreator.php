<?php

namespace Setwise\Helpers\Database\Schema\Support;

use PDO;
use PDOException;

class PostgresDatabaseCreator extends AbstractDatabaseCreator
{
    /**
     * Return a connection to the database service
     *
     * @param string $database
     * @return \PDO
     */
    public function connect($database = 'postgres') {
        $database = $database ?: 'postgres';
        $this->connection = new PDO(
            sprintf("%s:host=%s;port=%d;dbname=%s", $this->getDriverName(), $this->host, $this->port, $database), $this->username, $this->password, [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            ]
        );

        return $this->connection;
    }

    /**
     * Returns the name of the class's driver
     *
     * @return string
     */
    public function getDriverName() {
        return 'pgsql';
    }

    /**
     * Returns true if the database exists, false otherwise.
     *
     * @param string $database
     * @return boolean
     */
    public function exists(string $database) {
        $statement = $this->connection->prepare('SELECT * FROM pg_database WHERE datistemplate = false AND datname = ?');
        $statement->execute([$database]);

        return count($statement->fetchAll()) > 0;
    }

    /**
     * Create a given database name
     *
     * @param string $database
     * @return boolean
     */
    public function create($database) {
        try {
            $statement = $this->connection->prepare('CREATE DATABASE ' . $database);

            return $statement->execute();
        } catch (PDOException $exception) {
            return false;
        }
    }

    /**
     * Do any final configuration on a given database
     *
     * @param string $database
     * @return boolean
     */
    public function configure(string $database) {
        try {
            $this->connection->exec("CREATE EXTENSION IF NOT EXISTS Postgis");
        } catch (PDOException $exception) {
            return false;
        }

        return true;
    }
}
