<?php

namespace Setwise\Helpers\Database\Schema\Support;

/**
 * Base for all Database Creators used in InitializeDb.
 */
interface DatabaseCreatorContract
{
    /**
     * @param string $host
     * @param string $port
     * @param string $username
     * @param string $password
     */
    public function __construct(string $host, string $port, string $username, string $password);

    /**
     * Return a connection to the database service
     *
     * @return \PDO
     */
    public function connect($database);

    /**
     * Returns the name of the class's driver
     *
     * @return string
     */
    public function getDriverName();

    /**
     * Returns true if the database exists, false otherwise.
     *
     * @param string $database
     * @return boolean
     */
    public function exists(string $database);

    /**
     * Create a given database name
     *
     * @param string $database
     * @return boolean
     */
    public function create(string $database);

    /**
     * Do any final configuration on a given database
     *
     * @param string $database
     * @return boolean
     */
    public function configure(string $database);
}
