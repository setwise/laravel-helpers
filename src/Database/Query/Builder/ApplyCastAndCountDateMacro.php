<?php

namespace Setwise\Helpers\Database\Query\Builder;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Apply CastToDate Macro then group and order the output
 *
 * $outputField => date, week, month, year
 *
 * @mixin \Illuminate\Database\Schema\Blueprint
 *
 * @return $this
 */
class ApplyCastAndCountDateMacro
{
    /** @var array */
    protected $validTimeFrames = [
        'date', 'week', 'month', 'year',
    ];

    public function __invoke()
    {
        $validTimeFrames = $this->validTimeFrames;
        return function ($timeFrame = 'date', $field = 'created_at', $outputField = null, $countOutput='total') use ($validTimeFrames) {

            if (!in_array($timeFrame, $validTimeFrames)) {
                $timeFrame = 'date';
            }

            $outputField = $outputField ?: $timeFrame;

            return $this->addSelect(DB::Raw("Count(*) as {$countOutput}"))
                ->castToDate($timeFrame, $field, $outputField)
                ->groupBy($outputField)
                ->orderBy($outputField);
        };
    }
}
