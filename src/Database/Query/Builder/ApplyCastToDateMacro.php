<?php

namespace Setwise\Helpers\Database\Query\Builder;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Apply cast datetime macro
 *
 * $outputField => date, week:, month, year
 *
 * @mixin \Illuminate\Database\Schema\Blueprint
 *
 * @return $this
 */
class ApplyCastToDateMacro
{
    /** @var array */
    protected $validTimeFrames = [
        'date', 'week', 'month', 'year',
    ];

    public function __invoke()
    {
        $validTimeFrames = $this->validTimeFrames;
        return function ($timeFrame = 'date', $field = 'created_at', $outputField = null) use ($validTimeFrames) {

            if (!in_array($timeFrame, $validTimeFrames)) {
                $timeFrame = 'date';
            }

            $outputField = $outputField ?: $timeFrame;

            //default to date
            $q = "CAST({$field} as date) {$outputField}";

            //Week
            if ($timeFrame == 'week') {
                $q = "concat(cast(extract(year from {$field} ) as varchar), '-' , cast(extract(week from {$field} ) as varchar)) as {$outputField}";
            }

            //Month
            else if ($timeFrame == 'month') {
                $q = "concat(cast(extract(year from {$field} ) as varchar), '-' , cast(extract(month from {$field}) as varchar), '-1') as {$outputField}";
            }

            //Year
            else if ($timeFrame == 'year') {
                $q = "extract(year from {$field}) as {$outputField}";
            }

            return $this->addSelect(DB::Raw($q));
        };
    }
}
