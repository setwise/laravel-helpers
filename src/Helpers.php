<?php

namespace Setwise\Helpers;

class Helpers
{
    /**
     * Get the active user provided class
     *
     * @return \Illuminate\Foundation\Auth\User|string
     */
    public static function UserProviderClass()
    {
        $guard = config('auth.defaults.guard', 'web');
        $provider = config("auth.guards.{$guard}.provider", 'users');

        return config("auth.providers.{$provider}.model", '');
    }
}
