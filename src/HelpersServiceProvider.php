<?php

namespace Setwise\Helpers;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use Setwise\Helpers\Database\Query\Builder as BuilderMacros;
use Setwise\Helpers\Console\Commands as Commands;

class HelpersServiceProvider extends ServiceProvider
{
    /** @var array */
    protected $commands = [
        Commands\UserCommands\CheckUserEmailVerified::class,
        Commands\UserCommands\MarkAllUserEmailsVerified::class,
        Commands\UserCommands\MarkUserEmailNotVerified::class,
        Commands\UserCommands\MarkUserEmailVerified::class,
        Commands\InitializeDb::class,
        Commands\LogSeed::class,
    ];

    /** @var array */
    protected $builderMacros = [
        'castToDate' => BuilderMacros\ApplyCastToDateMacro::class,
        'castAndCountDate' => BuilderMacros\ApplyCastAndCountDateMacro::class,
        // 'sumGroup' => BuilderMacros\ApplySumGroupMacro::class,
        // 'countGroup' => BuilderMacros\ApplyCountGroupMacro::class
    ];

    /**
     * Bootstrap the application services
     */
    public function boot()
    {
        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }

        $this->registerBuilderMacros();
    }

    /**
     * Register the application services
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/helpers.php', 'helpers');

        // Register the service the package provides.
        $this->app->singleton(Helpers::class, function ($app) {
            return new Helpers;
        });
        $this->app->alias(Helpers::class, 'helpers');
    }

    /**
     * Console-specific booting
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__ . '/../config/helpers.php' => config_path('helpers.php'),
        ], 'helpers.config');

        // Registering package commands
         $this->commands($this->commands);
    }

    /**
     * Bootstrap the builder macros
     *
     * @return void
     */
    protected function registerBuilderMacros()
    {
        Collection::make($this->builderMacros)
            ->reject(function ($class, $macro) {
                Builder::hasMacro($macro);
            })
            ->each(function ($class, $macro) {
                Builder::macro($macro, app($class)());
            });
    }
}
