<?php

namespace Setwise\Helpers\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

trait ControllerHasUploads
{
    /** @var string */
    protected $uploadBucket = '';

    /** @var string */
    protected $uploadClass = '';

    /** @var string */
    protected $uploadAuthorize = '';

    /** @var string */
    protected $uploadRules = 'required|file|image|max:2048';

    /**
     * Upload an file
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|array
     */
    public function uploadFile(Request $request)
    {
        $this->authorize($this->uploadAuthorize, $this->uploadClass);

        $request->validate([
            'upload' => $this->uploadRules,
        ]);

        $file = $request->file('upload');
        $path = $file->storePublicly("bucket/{$this->uploadBucket}");
        $response = [
            'url' => Storage::url($path),
        ];

        // Normally this would be returned via $this->json
        // But since the ckeditor will be utilizing this link, we want to make sure the return format is expected
        return $request->header('REQUEST-OBJECT', null) == 'ckeditor' ?
            $response :
            $this->json($response);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteFile(Request $request)
    {
        $this->authorize($this->uploadAuthorize, $this->uploadClass);

        // Remove the storage's url from the path
        $path = str_replace(Storage::url(''), '', $request->input('url', ''));

        if (Storage::exists($path)) {
            Storage::delete($path);
        }

        return $this->json([], "Deleted file at {$path}");
    }
}
