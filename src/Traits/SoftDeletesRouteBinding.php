<?php

namespace Setwise\Helpers\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Str;

trait SoftDeletesRouteBinding
{
    /**
     * Make sure trashed entities get counted for in 'restore' based routes
     *
     * @param mixed $value
     * @param null $field
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function resolveRouteBinding($value, $field = null)
    {
        if (Str::contains(request()->route()->getName(), 'restore')) {
            return $this->withTrashed()->where($field ?? $this->getRouteKeyName(), $value)->first();
        } else {
            return parent::resolveRouteBinding($value, $field);
        }
    }

    /**
     * Retrieve the child model for a bound value, accounting for 'trashed' on restore
     *
     * @param string $childType
     * @param mixed $value
     * @param string|null $field
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function resolveChildRouteBinding($childType, $value, $field)
    {
        if (Str::contains(request()->route()->getName(), 'restore')) {
            $relationship = $this->{Str::plural(Str::camel($childType))}();

            $relationship->withTrashed();

            if ($relationship instanceof HasManyThrough ||
                $relationship instanceof BelongsToMany
            ) {
                return $relationship->where($relationship->getRelated()->getTable() . '.' . $field, $value)->first();
            } else {
                return $relationship->where($field, $value)->first();
            }
        } else {
            return parent::resolveChildRouteBinding($childType, $value, $field);
        }
    }
}
